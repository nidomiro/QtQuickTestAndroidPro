#ifndef TESTLISTVIEWCPPOBJ_H
#define TESTLISTVIEWCPPOBJ_H

#include <QObject>
#include <QStringListModel>

class TestListViewCppObj : public QStringListModel
{
    Q_OBJECT
public:
    TestListViewCppObj(QObject *parent = Q_NULLPTR):
        QStringListModel(parent){}

    TestListViewCppObj(const QStringList &strings, QObject *parent = Q_NULLPTR):
        QStringListModel(strings, parent) {}


public slots:

    void addItem(const QString &title);

private:
    Q_DISABLE_COPY(TestListViewCppObj)

};



Q_DECLARE_METATYPE(TestListViewCppObj*)

#endif // TESTLISTVIEWCPPOBJ_H
