#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QQuickView>
#include <QQmlContext>
#include <QVariant>
#include <QtQuickControls2>
#include "listitemdataobject.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QList<QObject*> dataList;

    QVector<QString> colors({
                             "lightblue",
                              "tomato",
                              "yellowgreen",
                              "steelblue"
                          });

    for(int i=0; i < 10; i++)
    {
        dataList.append(new ListItemDataObject("Test" + QString::number(i), colors[i % colors.length()]));
    }


    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("myTestModel", QVariant::fromValue(dataList));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;




//  QQuickView *addButtonView = engine.rootContext()->findChild<QQuickView*>("addItemButton");


    return app.exec();
}
