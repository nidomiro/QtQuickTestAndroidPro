#include "listitemdataobject.h"

ListItemDataObject::ListItemDataObject(QObject *parent) : QObject(parent)
{

}

ListItemDataObject::ListItemDataObject(const QString &name, const QString &color, QObject *parent)
    :name(name),
      color(color)
{

}

QString ListItemDataObject::getName() const
{
    return name;
}

void ListItemDataObject::setName(const QString &value)
{
    name = value;
    emit nameChanged(value);
}

QString ListItemDataObject::getColor() const
{
    return color;
}

void ListItemDataObject::setColor(const QString &value)
{
    color = value;
    emit colorChanged(value);
}
