import QtQuick 2.9
import QtQuick.Controls 2.2

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 640
    height: 640
    title: qsTr("Scroll")


    function randomColor() {
        var hue = Math.random()
        var golden_ratio_conjugate = 0.618033988749895
        hue += golden_ratio_conjugate
        hue %= 1
        return Qt.hsva(hue, 0.5, 0.95, 1)
    }


    ScrollView {
        id: testListViewScrollView

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: addItemTextEdit.left
        anchors.rightMargin: 10

        //contentHeight: testListView.height


        ListView {
            id: testListView
            objectName: "testListView"
            model: myTestModel
            delegate: Rectangle {

                width: parent.width
                height: 25
                color: model.modelData.color

                MouseArea {
                    anchors.fill: parent
                    onClicked: { parent.color = randomColor() }
                }

                Text {
                    x: 5
                    anchors.verticalCenter: parent.verticalCenter
                    y: (parent.height - this.height)/2
                    text: testListView.objectName + ": " + name

                }
            }
        }
    }


    TextField {
        id: addItemTextEdit
        //width: 100

        anchors.right: addItemButton.left
        anchors.rightMargin: 5
        anchors.bottom: addItemPreview.top
        anchors.bottomMargin: 5

        //width: 200

        text: qsTr("Text Field")
    }

    Text {
        id: addItemPreview

        //anchors.top: addItemTextEdit.bottom
        //anchors.topMargin: 5
        //anchors.left: testListViewScrollView.right

        anchors.left: addItemTextEdit.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10

        text: addItemTextEdit.text
        font.pixelSize: 12
    }



    Button {
        id: addItemButton
        objectName: "addItemButton"
        anchors.top: addItemTextEdit.top
        //anchors.left: addItemTextEdit.right
        //anchors.leftMargin: 5

        anchors.right: parent.right
        anchors.rightMargin: 10


        text: qsTr("Add Item")
        onClicked: testListView.model.addItem(addItemTextEdit.text)
    }

    CheckBox {
        id: checkBox
        y: 15
        text: qsTr("Check Box")
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    CheckDelegate {
        id: checkDelegate
        y: 58
        text: qsTr("Check Delegate")
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    RadioButton {
        id: radioButton
        y: 112
        text: qsTr("Radio Button")
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    SpinBox {
        id: spinBox
        y: 163
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    Slider {
        id: slider
        y: 236
        anchors.right: parent.right
        anchors.rightMargin: 10
        value: 0.5
    }

    RoundButton {
        id: roundButton
        x: 582
        y: 302
        text: "+"
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    ProgressBar {
        id: progressBar
        y: 280
        height: 5
        clip: false
        anchors.right: parent.right
        anchors.rightMargin: 10
        value: slider.value
    }

    Switch {
        id: switch1
        x: 522
        y: 356
        text: qsTr("Switch")
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    RangeSlider {
        id: rangeSlider
        x: 430
        y: 394
        anchors.right: parent.right
        anchors.rightMargin: 10
        second.value: 0.75
        first.value: 0.25
    }

    BusyIndicator {
        id: busyIndicator
        x: 570
        y: 410
        width: 60
        height: 60
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    ComboBox {
        id: comboBox
        x: 512
        y: 500
        anchors.right: parent.right
        anchors.rightMargin: 10

        model: [ "Banana", "Apple", "Coconut" ]
    }




}
