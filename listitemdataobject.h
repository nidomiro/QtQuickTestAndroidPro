#ifndef LISTITEMDATAOBJECT_H
#define LISTITEMDATAOBJECT_H

#include <QObject>

class ListItemDataObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString color READ getColor WRITE setColor NOTIFY colorChanged)


public:
    explicit ListItemDataObject(QObject *parent = nullptr);
    explicit ListItemDataObject(const QString &name, const QString &color, QObject *parent = nullptr);

    QString getName() const;
    void setName(const QString &value);

    QString getColor() const;
    void setColor(const QString &value);

signals:
    void nameChanged(const QString &name);
    void colorChanged(const QString &color);

public slots:


private:
    QString name;
    QString color;
};

#endif // LISTITEMDATAOBJECT_H
